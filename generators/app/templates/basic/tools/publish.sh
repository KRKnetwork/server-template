#!/bin/bash
# Version 2024-01-22
ScriptPath="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/../"
cd $ScriptPath

version="$(echo $(git describe --tags) | sed 's/v//g')"
built=$built
branch="$(git rev-parse --abbrev-ref HEAD)"

while getopts ulr: flag
do
  case "${flag}" in
    r) registry=${OPTARG};;
    l) isLatest=1;;
    u) ignoreUntracked=1;;
  esac
done

# Check if Registry is provided
[ -z "$registry" ] && { echo -e "\e[31m ✖  Registry (-r) is not set!\e[0m"; exit; }

function echoResult() {
  echo -e "\e[32m ✔  Pushed version \e[1m$version\e[0m\e[32m to \e[1m$1\e[0m"
}

# latest
if [[ "${isLatest}" -eq 1 ]]
then
  docker build \
    --pull \
    --build-arg BUILT=$built \
    --build-arg VERSION=$version \
    -t $registry:latest \
    .

  docker push $registry:latest

  echoResult "latest"
  exit
fi

if [[ "${ignoreUntracked}" -ne 1 ]]
then
  # Check for untracked changes
  if ! git diff-index --quiet HEAD --
  then
    echo -e "\e[31m ✖  Untracked Changes! \e[33mExecute with -u to ignore.\e[0m"
    exit
  fi
fi

docker images | grep $registry | tr -s ' ' | cut -d ' ' -f 3 | xargs -I {} docker rmi -f {}


# check if $branch is not "main" or "master"
isMain=$(echo $branch | grep -E '^(main|master)$' | wc -l)

# branch
if [[ "${isMain}" -eq 0 ]]
then
  git push --set-upstream origin $branch

  docker build \
    --pull \
    --build-arg BUILT=$built \
    --build-arg VERSION=$version \
    -t $registry:$branch \
    .

  docker push $registry:$branch

  echoResult $branch
  exit
fi

# Check if latest commit is tagged
if ! [[ `git describe --exact-match HEAD` ]] &>/dev/null
then
  echo -e "\e[31m ✖  Latest Commit is not tagged!\e[0m"
  exit
fi

# main
git push && git push --tags

docker build \
  --pull \
  --build-arg BUILT=$built \
  --build-arg VERSION=$version \
  --label "version=$version" \
  -t $registry:$version \
  -t $registry:latest \
  -t $registry:main \
  .

docker push --all-tags $registry
echoResult "$version, main, latest"
