const path = require('path')
const Profiler = require('@krknet/profiler')
const exec = require('child_process').exec // eslint-disable-line security/detect-child-process

global.__basePath = path.join(__dirname, '../../app/')
global.__environment = process.env.NODE_ENV || 'development'
global.__dataFolder = path.join(process.cwd(), 'data/')
global.__config = require('>/config/')

const { Connection } = require('@krknet/mongo')
global.__db = new Connection({ url: global.__config.mongo })

async function run () {
  // const param = (process.argv.find(d => d.startsWith('--param=')) || '').replace('--param=', '').trim()
  // if (!param) return Profiler.error('Paramater param is required! (--param=XXX)')

  // await global.__db.connect()
  console.log(await getVersion())
  // await global.__db.disconnect()
}

run()

async function getVersion () {
  const shell = cmd => new Promise((resolve, reject) => { exec(cmd, (_err, stdout) => resolve(stdout)) })

  const [built, version] = await Promise.all([
    shell("date '+%d.%m.%Y'").then(d => d.replace('\n', '')),
    shell('echo $(git describe --tags) | sed "s/v//g"').then(d => d.replace('\n', ''))
  ])

  return {
    version: process.env.CONTAINER_version || version,
    mode: global.__environment,
    built: process.env.CONTAINER_built || built
  }
}
