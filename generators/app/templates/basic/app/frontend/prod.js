const fs = require('fs')
const path = require('path')
const { Router } = require('@krknet/http-server')
const Profiler = require('@krknet/profiler')
const _ = require('lodash')

const pathPages = path.join(__dirname, 'pages')
const pathRenders = path.resolve(__dirname, '../static/renders/')
const page404 = path.join(__dirname, 'layout/404.html')

module.exports = class PagesRouter {
  constructor () {
    this.router = new Router()
  }

  emit (...args) {
    if (global.__socketServer) global.__socketServer.emit(...args)
  }

  async start (onUpdate) {
    if (!onUpdate || typeof onUpdate !== 'function') throw new Error('PagesRouter: onUpdate-Function is required for start')

    this.emit('rendering')
    await this._getRedirects()
    await this._getPages()

    this.__onUpdate = onUpdate
    await this._rebuildRouter()
    this.emit('update')
  }

  async _rebuildRouter () {
    const router = new Router()

    for (const redirect of this.redirects) router.get(redirect.from, (req, res) => res.redirect(redirect.to))

    for (const page of this.pages) {
      router.get(page.route, async (req, res) => {
        res
          .status(200)
          .set({ 'Content-Type': 'text/html' })
          .end(page.html)
      })
    }

    router.get('*', (req, res) => res.sendFile(page404))

    this.router = router
    this.__onUpdate(this.router)
  }

  _getRedirects () {
    const filePath = path.join(__dirname, 'pages/redirects.json')
    this.redirects = fs.existsSync(filePath) ? JSON.parse(fs.readFileSync(filePath, 'utf8')) : []
  }

  async _getPages () {
    this.pages = []
    if (!fs.existsSync(pathPages) || !fs.statSync(pathPages).isDirectory()) return
    const profiler = new Profiler('Pages rendering', 3).start()

    const pages = fs.readdirSync(pathPages).map(d => mapPage(d)).flat().filter(Boolean)
    for (const page of pages) {
      profiler.step(page.page)

      try {
        const def = await renderPage(page)
        if (def) this.pages.push(def)
      } catch (err) {
        profiler.fail(err.message)
        throw err
      }
    }

    profiler.succeed()

    return profiler
  }

  getRouter () {
    return this.router
  }
}

function mapPage (page, _subpath, subroute) {
  const subpath = _subpath || []
  const pages = []
  const dirPath = path.join(pathPages, subpath.join('/') || '', page)
  const manifestPath = path.join(dirPath, 'manifest.json')
  if (!fs.statSync(dirPath).isDirectory() || !fs.existsSync(manifestPath)) return pages

  const manifest = JSON.parse(fs.readFileSync(manifestPath, 'utf8'))
  const route = `${subroute || ''}/${manifest.route || page}/`.replace(/\/(\/)+/g, '/')

  if (manifest.subpages) pages.push(...fs.readdirSync(dirPath).map(d => mapPage(d, [...subpath, page], route)).flat().filter(d => d))

  if (!manifest.active) return pages

  const vuePath = path.join(dirPath, 'Page.vue')
  const pugPath = path.join(dirPath, 'template.pug')
  const entryPoint = fs.existsSync(vuePath) ? vuePath : (fs.existsSync(pugPath) ? pugPath : false)

  if (!entryPoint) return pages

  const pageTitle = [...subpath, page].join('-')
  pages.push({
    ...manifest,
    page: pageTitle,
    entryPoint,
    route
  })

  return pages
}

async function renderPage (page) {
  const locals = {
    _config: global.__config,
    _page: page
  }

  const rawHTML = await fs.promises.readFile(path.resolve(pathRenders, `page-${page.page}.html`), 'utf8')
  const compiled = _.template(rawHTML)
  return {
    ...page,
    html: compiled(locals)
  }
}
