const fs = require('fs')
const path = require('path')
const pug = require('pug')
const { Router } = require('@krknet/http-server')
const Profiler = require('@krknet/profiler')

const pathComponents = path.join(__dirname, 'components/global')
const pathPages = path.join(__dirname, 'pages')
const pathTemplate = path.join(__dirname, 'layout/layout.pug')
const page404 = path.join(__dirname, 'layout/404.html')

module.exports = class PagesRouter {
  #server

  constructor ({ httpServer } = {}) {
    this.router = new Router()
    this.#server = httpServer

    this.pages = []
    this.components = []
    this.redirects = []
  }

  emit (...args) {
    if (global.__socketServer) global.__socketServer.emit(...args)
  }

  async start (onUpdate) {
    if (!onUpdate || typeof onUpdate !== 'function') throw new Error('PagesRouter: onUpdate-Function is required for start')

    this.emit('rendering')
    await this._getRedirects()
    await this._getComponents()
    await this._getPages()

    this.__onUpdate = onUpdate
    await this._rebuildRouter()
    this.emit('update')
  }

  async _rebuildRouter () {
    const router = new Router()

    for (const redirect of this.redirects) router.get(redirect.from, (req, res) => res.redirect(redirect.to))

    const vite = await import('vite') // eslint-disable-line node/no-unsupported-features/es-syntax, node/no-unpublished-import
    const viteInstance = await vite.createServer({
      appType: 'custom',
      root: __dirname,
      configFile: path.resolve(global.__basePath, '../build/vite.config.mjs'),
      base: '/_vite_/',
      logLevel: 'info',
      clearScreen: false,
      plugins: [
        (() => ({
          name: 'static-resources-replacement',
          enforce: 'post',
          transformIndexHtml: html => html.replace(/<!-- (.+?\/static\/.+?)-->/g, (_, p1) => pug.render(p1))
        }))()
      ],
      server: {
        middlewareMode: true,
        hmr: {
          server: this.#server.rawServer,
          clientPort: this.#server.options.port
        }
      }
    })

    for (const page of this.pages) {
      router.get(page.route, async (req, res) => {
        const [url] = req.originalUrl.split('?')
        const html = await viteInstance.transformIndexHtml(url, page.func())
        res
          .status(200)
          .set({ 'Content-Type': 'text/html' })
          .end(html)
      })
    }

    router.use(viteInstance.middlewares)

    router.get('*', (req, res) => res.sendFile(page404))

    this.router = router
    this.__onUpdate(this.router)
  }

  _getRedirects () {
    const filePath = path.join(__dirname, 'pages/redirects.json')
    this.redirects = fs.existsSync(filePath) ? JSON.parse(fs.readFileSync(filePath, 'utf8')) : []
  }

  async _getComponents () {
    this.components = []
    if (!fs.existsSync(pathComponents) || !fs.statSync(pathComponents).isDirectory()) return
    const profiler = new Profiler('Global Components rendering', 3).start()
    const camelize = input => input.replace(/-./g, x => x[1].toUpperCase())
    const kebabize = input => input.replace(/[A-Z]/g, x => `-${x[0].toLowerCase()}`)

    const components = fs.readdirSync(pathComponents)
    for (const component of components) {
      let title, file

      if (fs.statSync(path.join(pathComponents, component)).isDirectory()) {
        file = path.join(pathComponents, component, 'index.vue')
        if (!fs.existsSync(file)) continue
        title = component.split('/').reverse()[0]
      } else {
        if (!component.endsWith('.vue')) continue
        title = component.split('/').reverse()[0].replace('.vue', '')
        file = path.join(pathComponents, component)
      }
      if (!title) continue

      this.components.push({ title: kebabize(title), file, speaking: camelize(title) })
      profiler.step(title)
    }

    profiler.succeed()

    return profiler
  }

  async _getPages () {
    this.pages = []
    if (!fs.existsSync(pathPages) || !fs.statSync(pathPages).isDirectory()) return
    const profiler = new Profiler('Pages rendering', 3).start()

    const pages = fs.readdirSync(pathPages).map(d => mapPage(d)).flat().filter(Boolean)
    for (const page of pages) {
      profiler.step(page.page)

      try {
        const def = await renderPage(page, { components: this.components })
        if (def) this.pages.push(def)
      } catch (err) {
        profiler.fail(err.message)
        throw err
      }
    }

    profiler.succeed()

    return profiler
  }

  getRouter () {
    return this.router
  }
}

function mapPage (page, _subpath, subroute) {
  const subpath = _subpath || []
  const pages = []
  const dirPath = path.join(pathPages, subpath.join('/') || '', page)
  const manifestPath = path.join(dirPath, 'manifest.json')
  if (!fs.statSync(dirPath).isDirectory() || !fs.existsSync(manifestPath)) return pages

  const manifest = JSON.parse(fs.readFileSync(manifestPath, 'utf8'))
  const route = `${subroute || ''}/${manifest.route || page}/`.replace(/\/(\/)+/g, '/')

  if (manifest.subpages) pages.push(...fs.readdirSync(dirPath).map(d => mapPage(d, [...subpath, page], route)).flat().filter(d => d))

  if (!manifest.active) return pages

  const vuePath = path.join(dirPath, 'Page.vue')
  const pugPath = path.join(dirPath, 'template.pug')
  const entryPoint = fs.existsSync(vuePath) ? vuePath : (fs.existsSync(pugPath) ? pugPath : false)

  if (!entryPoint) return pages

  const pageTitle = [...subpath, page].join('-')
  pages.push({
    ...manifest,
    page: pageTitle,
    entryPoint,
    route
  })

  return pages
}

async function renderPage (page, { components }) {
  const locals = {
    _config: global.__config,
    _page: page,
    _components: components,
    _dependencies: page.dependencies || []
  }

  return {
    ...page,
    html: pug.renderFile(pathTemplate, locals),
    func: () => pug.renderFile(pathTemplate, locals)
  }
}
