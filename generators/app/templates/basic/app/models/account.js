const { BaseModel } = require('@krknet/mongo')

module.exports = class Account extends BaseModel {
  static get collection () { return global.__db.collection('accounts') }
  static get _modelSchema () { return schema }
}

const schema = {
  username: {
    type: String,
    title: 'Username',
    required: true,
    transformations: ['trim', 'longer0orNull']
  },
  firstname: {
    type: String,
    title: 'Firstname',
    required: true,
    transformations: ['trim', 'longer0orNull']
  },
  lastname: {
    type: String,
    title: 'Lastname',
    required: true,
    transformations: ['trim', 'longer0orNull']
  }
}
