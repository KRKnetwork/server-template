const { STATUS_CODES } = require('http')
const Sentry = require('@sentry/node')
require('@sentry/tracing')

const enableCapture = global.__environment !== 'development'
const tracesSampleRate = (rawRate => Number.isNaN(rawRate) ? 0 : Math.max(0, rawRate))(Number(global.__config.tracingRate || 0))

if (enableCapture) {
  Sentry.init({
    release: `<%= name %>@${process.env.CONTAINER_version || 'dev'}`,
    dsn: global.__config.sentryDSN,
    tracesSampler: samplingContext => {
      const traceName = samplingContext.transactionContext.name
      if (traceName.startsWith('GET /') && !traceName.startsWith('GET /api/')) return 0
      return tracesSampleRate
    }
  })
}
const captureException = enableCapture ? Sentry.captureException : console.log

module.exports = {
  setUser: Sentry.setUser,
  captureException,
  httpRequests: [
    Sentry.Handlers.requestHandler(),
    Sentry.Handlers.tracingHandler()
  ],
  httpErrors: [
    (err, req, res, next) => {
      if (err.status) return next(err)

      const code = Number.parseInt(err.message)
      if (Number.isNaN(code)) return next(err)
      if (code.toString() !== err.message) return next(err)

      err.status = code
      next(err)
    },
    Sentry.Handlers.errorHandler(),
    (req, res, next) => {
      const err = new Error('404')
      err.status = 404
      next(err)
    },
    (err, req, res, next) => {
      if (!enableCapture) captureException(err)
      const code = err?.status || 500
      const message = STATUS_CODES[code] || 'unknown'

      res
        .status(code)
        .json({
          message,
          code
        })
    }
  ]
}
