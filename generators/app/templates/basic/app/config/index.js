const mainConfig = require('./main.config.json')
const devConfig = global.__environment !== 'development' ? {} : require('./dev.config.json')
const envConfig = readEnvironment('<%= envprefix %>')

module.exports = Object.freeze({
  ...mainConfig,
  ...devConfig,
  ...envConfig
})

function readEnvironment (key) {
  const prefix = `${key}_`
  return Object.keys(process.env)
    .filter(d => d.startsWith(prefix))
    .reduce((acc, curr) => {
      acc[curr.replace(prefix, '')] = enrichVariable(process.env[curr])
      return acc
    }, {})

  function enrichVariable (variable) {
    try {
      return JSON.parse(variable)
    } catch (_) {
      return variable
    }
  }
}

// function resolveBooleanKey (field, defaultValue) {
//   if (field === undefined) return defaultValue
//   if (typeof field === 'boolean') return field
//   if (typeof field === 'string') {
//     switch (field.toLowerCase()) {
//       case 'false':
//       case '0':
//         return false
//       case 'true':
//       case '1':
//         return true
//     }
//   }
//   if (field === 0) return false
//   if (field === 1) return true

//   return defaultValue
// }
