const path = require('path')
const { EventEmitter } = require('events')

global.__environment = process.env.NODE_ENV || 'development'
global.__basePath = __dirname
global.__dataFolder = path.join(process.cwd(), 'data/')
global.__config = require('./config/')

const { httpRequests, httpErrors } = require('./libraries/errorhandling')

const { Connection } = require('@krknet/mongo')
const HTTPserver = require('@krknet/http-server')
const SocketServer = require('@krknet/socket-server')

const initiatorsDB = [
  // connection => connection.get('campaigns').createIndex({ status: 1 }),
]
const initiatorsSocket = []

global.__eventBus = new EventEmitter()
global.__db = new Connection(global.__config.mongo, { initiatiors: initiatorsDB })

global.__httpServer = new HTTPserver({
  host: '0.0.0.0',
  port: global.__config.port,
  ApiRouter: require('>/api/'),
  PagesRouter: require('>/frontend/'),
  loadingPage: path.join(__dirname, 'frontend/layout/loading.html'),
  sentryRequests: httpRequests,
  sentryErrors: httpErrors,
  doTrustProxy: true,
  doLogAPI: global.__environment === 'development',
  statics: [
    { route: '/static/', path: path.join(global.__basePath, 'static') },
    { route: '/static/axios/', path: path.join(global.__basePath, '../node_modules/axios/dist/') },
    { route: '/static/sockets/', path: path.join(global.__basePath, '../node_modules/socket.io/client-dist/') },
    { route: '/static/vue/', path: path.join(global.__basePath, '../node_modules/vue/dist/') },
    { route: '/static/vue/components', path: path.join(global.__dataFolder, 'renders') }
  ]
})

global.__socketServer = new SocketServer(global.__httpServer.rawServer, { initiatiors: initiatorsSocket })

// Start Server
async function start () {
  try {
    await global.__db.connect()

    await global.__socketServer.start()
    await global.__httpServer.start()
  } catch (err) {
    console.log(err)
    console.log('')
    await stop('Failure')
    throw new Error(err)
  }

  if (typeof process.send === 'function') process.send('ready')

  process.on('SIGINT', () => stop('SIGINT signal received'))
  process.on('SIGTERM', () => stop('SIGTERM signal received'))
  process.on('message', msg => { if (msg === 'shutdown') stop() })
}

async function stop (message) {
  if (message) console.log(` ☀  ${message}`)

  try {
    await global.__httpServer.stop()
    await global.__socketServer.stop()
    await global.__db.disconnect()
    process.exit(1) // eslint-disable-line no-process-exit
  } catch (error) {
    console.log(error)
    console.log(' ✗  Error during Shutdown')
    process.exit(0) // eslint-disable-line no-process-exit
  }
}

start()
