const { Router } = require('@krknet/http-server')
const fs = require('fs')
const path = require('path')
const cors = require('cors')
const exec = require('child_process').exec

const systemStatus = {
  socket: false,
  api: false,
  frontend: false
}

module.exports = class ApiRouter {
  constructor (logger) {
    this.router = new Router()

    systemStatus.socket = global.__socketServer ? global.__socketServer.isRunning : false
    systemStatus.api = global.__httpServer ? global.__httpServer.isAPIRunning : false
    systemStatus.frontend = global.__httpServer ? global.__httpServer.isFrontendRunning : false
    this.updateString()

    global.__eventBus
      .on('system', (component, status) => {
        if (systemStatus[component] === undefined) return
        systemStatus[component] = status === 'up'
        this.updateString()
      })

    this.infoString = ''
    this.versionInfo = {}
    this.getVersion()

    if (logger) this.router.use(logger)
    this.router.use(cors({ allowedHeaders: ['sentry-trace', 'baggage'] }))

    this.router.get('/', (req, res) => {
      res.set('Content-Type', 'application/json; charset=utf-8').send(this.infoString)
    })

    this.map()

    this.router.use((req, res, next) => {
      const err = new Error('404')
      err.status = 404
      next(err)
    })
  }

  map () {
    const folders = fs.readdirSync(__dirname)
    for (const entry of folders) {
      const controllerPath = path.join(__dirname, entry)
      const routeName = `/${entry.replace('.js', '')}/`

      if (routeName === '/index/') continue

      this.router.use(routeName, require(controllerPath))
    }
  }

  getRouter () {
    return this.router
  }

  async getVersion () {
    const [built, version] = await Promise.all([
      shell("date '+%d.%m.%Y'").then(d => d.replace('\n', '')),
      shell('echo $(git describe --tags) | sed "s/v//g"').then(d => d.replace('\n', ''))
    ])

    this.versionInfo = {
      version: process.env.CONTAINER_version || version,
      mode: global.__environment,
      built: process.env.CONTAINER_built || built
    }

    this.updateString()
  }

  updateString () {
    this.infoString = JSON.stringify({
      ...this.versionInfo,
      ...systemStatus
    })
  }
}

const shell = cmd => new Promise((resolve, reject) => { exec(cmd, (_err, stdout) => resolve(stdout)) }) // eslint-disable-line security/detect-child-process
