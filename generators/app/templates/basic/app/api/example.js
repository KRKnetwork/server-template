const router = require('@krknet/http-server').Router()

router
  .get('/', (req, res) => {
    res.json(true)
  })

module.exports = router
