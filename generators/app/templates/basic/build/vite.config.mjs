/* eslint-disable node/no-unpublished-import */
/* global __dirname */

import vuePlugin from '@vitejs/plugin-vue'
import WindiCSS from 'vite-plugin-windicss'
import { resolve } from 'path'

export default {
  define: {
    __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: 'false'
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, '../app/frontend/components/shared/')
    }
  },
  plugins: [
    vuePlugin(),
    WindiCSS({ configFiles: [resolve(__dirname, 'windi.production.config.js')] })
  ]
}
