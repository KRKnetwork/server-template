const { resolve } = require('path')

const baseConfig = require('./windi.config.js')

module.exports = {
  ...baseConfig,
  extract: {
    include: [
      `${resolve(__dirname, '../app/frontend/')}/**/*.{vue,html,pug,js,css}`
    ]
  }
}
