/* eslint-disable node/no-missing-import, node/no-unpublished-import */

import path from 'path'
import * as vite from 'vite'
import pug from 'pug'
import fs from 'fs'
import { URL } from 'url'
import { sentryVitePlugin } from '@sentry/vite-plugin'

const __dirname = new URL('.', import.meta.url).pathname
const relative = p => path.relative(process.cwd(), p)

const pathFrontend = path.resolve(__dirname, '../app/frontend/')
const pathPages = path.join(pathFrontend, 'pages')
const pathLayout = path.join(pathFrontend, 'layout/layout.pug')
const pathComponents = path.join(pathFrontend, 'components/global')
const pathOutput = path.resolve(__dirname, '../app/static/renders/')

async function run () {
  const flagIndex = process.argv.indexOf('-p')
  const projectID = flagIndex >= 0 ? (process.argv[flagIndex + 1] || '').trim() : ''
  if (!projectID) throw new Error('No project ID provided')

  const pages = fs
    .readdirSync(pathPages)
    .map(d => mapPage(d))
    .flat()
    .filter(Boolean)

  const components = await getComponents()

  return vite.build({
    root: pathFrontend,
    configFile: path.resolve(__dirname, 'vite.config.mjs'),
    base: '/static/renders/',
    plugins: [
      (() => ({
        name: 'static-resources-replacement-pre',
        enforce: 'pre',
        resolveId: id => id.startsWith('/static/') ? id.replace('/static/', '/') : null
      }))(),
      pagesPlugin({ components, pages }),
      (() => ({
        name: 'static-resources-replacement-post',
        enforce: 'post',
        transformIndexHtml: html => html
          .replace(/<!-- (.+?\/static\/.+?)-->/g, (_, p1) => pug.render(p1))
          .replace(/\/static\/renders\/(.+?\/)/g, (fullMatch, p1) => p1 === 'assets/' ? fullMatch : '/static/renders/')
      }))(),
      sentryVitePlugin({
        include: pathOutput,
        url: 'https://sentry.krknet.de/',
        release: {
          name: `${projectID}@${process.env.CONTAINER_version || 'dev'}`
        },
        authToken: process.env.SENTRY_AUTH_TOKEN,
        telemetry: false,
        urlPrefix: '~/static/renders/'
      })
    ],
    publicDir: '../static/',
    build: {
      outDir: pathOutput,
      emptyOutDir: true,
      sourcemap: true,
      manifest: true,
      rollupOptions: {
        input: Object.fromEntries(pages.map(d => [`page-${d.page}`, path.resolve(pathFrontend, `page-${d.page}.html`)]))
      }
    }
  })
}

function mapPage (page, _subpath, subroute) {
  const subpath = _subpath || []
  const pages = []
  const dirPath = path.join(pathPages, subpath.join('/') || '', page)
  const manifestPath = path.join(dirPath, 'manifest.json')
  if (!fs.statSync(dirPath).isDirectory() || !fs.existsSync(manifestPath)) return pages

  const manifest = JSON.parse(fs.readFileSync(manifestPath, 'utf8'))
  const route = `${subroute || ''}/${manifest.route || page}/`.replace(/\/(\/)+/g, '/')

  if (manifest.subpages) pages.push(...fs.readdirSync(dirPath).map(d => mapPage(d, [...subpath, page], route)).flat().filter(d => d))

  if (!manifest.active) return pages

  const vuePath = path.join(dirPath, 'Page.vue')
  const pugPath = path.join(dirPath, 'template.pug')
  const entryPoint = fs.existsSync(vuePath) ? vuePath : (fs.existsSync(pugPath) ? pugPath : false)

  if (!entryPoint) return pages

  const pageTitle = [...subpath, page].join('-')
  pages.push({
    ...manifest,
    page: pageTitle,
    entryPoint,
    route
  })

  return pages
}

async function getComponents () {
  const output = []
  if (!fs.existsSync(pathComponents) || !fs.statSync(pathComponents).isDirectory()) return
  const camelize = input => input.replace(/-./g, x => x[1].toUpperCase())
  const kebabize = input => input.replace(/[A-Z]/g, x => `-${x[0].toLowerCase()}`)

  const components = await fs.promises.readdir(pathComponents)
  for (const component of components) {
    let title, file

    if ((await fs.promises.stat(path.join(pathComponents, component))).isDirectory()) {
      file = path.join(pathComponents, component, 'index.vue')
      if (!fs.existsSync(file)) continue
      title = component.split('/').reverse()[0]
    } else {
      if (!component.endsWith('.vue')) continue
      title = component.split('/').reverse()[0].replace('.vue', '')
      file = path.join(pathComponents, component)
    }
    if (!title) continue

    output.push({ title: kebabize(title), file, speaking: camelize(title) })
  }

  return output
}

const pagesPlugin = options => {
  const pages = Object.fromEntries(options.pages.map(d => [d.page, d]))
  return {
    name: 'vite-plugin-pages',

    resolveId (id) {
      if (path.extname(id) === '.html') {
        const relativeId = relative(id)
        const pageName = path.posix.join(path.dirname(relativeId), path.basename(relativeId, '.html')).split('page-').pop()

        const page = pages[pageName]
        if (page) return id
      }

      return null
    },

    load (id) {
      if (path.extname(id) === '.html') {
        const relativeId = relative(id)
        const pageName = path.posix.join(path.dirname(relativeId), path.basename(relativeId, '.html')).split('page-').pop()

        const page = pages[pageName]
        if (page) {
          const locals = {
            _config: global.__config,
            _page: page,
            _components: options.components,
            _dependencies: page.dependencies || [],
            _isBuild: true
          }

          return pug.renderFile(pathLayout, locals)
        }
      }

      return null
    }
  }
}

run()
