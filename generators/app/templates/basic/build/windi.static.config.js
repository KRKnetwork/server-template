const { resolve } = require('path')

const baseConfig = require('./windi.config.js')

module.exports = {
  ...baseConfig,
  extract: {
    include: [
      `${resolve(__dirname, '../app/frontend/layout/')}/*.html`,
      `${resolve(__dirname, '../app/libraries/generators/')}/**/*.{pug,css}`
    ]
  }
}
