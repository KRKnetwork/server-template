const colors = require('windicss/colors')
// const defaultConfig = require('windicss/defaultConfig')

module.exports = {
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter', 'sans-serif']
      },
      zIndex: {
        '-1': -1,
        90: 90,
        100: 100
      },
      colors: {
        primary: colors.fuchsia
      }
    }
  },
  plugins: [
    require('windicss/plugin/forms')
  ]
}
