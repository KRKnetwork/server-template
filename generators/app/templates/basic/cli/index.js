const path = require('path')
const fs = require('fs')

global.__environment = process.env.NODE_ENV || 'development'
global.__basePath = path.join(__dirname, '../app/')
global.__dataFolder = path.join(process.cwd(), 'data/')
global.__config = require('>/config/')

const allModels = fs.readdirSync(path.join(__dirname, '../app/models/'))

const { Connection, CLIservice } = require('@krknet/mongo')
global.__db = new Connection({ url: global.__config.mongo, doLog: false })

const service = new CLIservice({
  dbConnection: global.__db,
  models: allModels.map(d => require(`>/models/${d}`))
})

service.query(process.argv.slice(2))
