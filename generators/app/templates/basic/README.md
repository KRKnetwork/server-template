# <%= title %>

<img width="200px" src="./logo.png">

> Tolle Beschreibung

## Local Usage

```bash
npm i

npm start
```

## Production Usage
Create a dedicated folder for each container.

config.env
```env
NODE_ENV=production
<%= envprefix %>_mongo=mongodb:27017/<%= mongoname %>
```

update.sh
```bash
#!/bin/bash
# Version 2024-01-28
containerName="<%= name %>"
registryURL="<%= dockerregistry %>"
branch=main
port=8080
portalURL=""

while getopts b: flag
do
  case "${flag}" in
    b) branch=${OPTARG};;
  esac
done

ScriptPath="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/"

echo -e "\e[33m >  Updating from branch \e[1m$branch\e[0m\e[33m on \e[1m$registryURL\e[0m"
docker pull -q $registryURL:$branch

docker stop $containerName > /dev/null 2>&1 || true
docker rm $containerName > /dev/null 2>&1 || true
echo -e "\e[32m ✔  Stopping running instance\e[0m"

docker run \
  --restart always \
  -p 127.0.0.1:$port:8080 \
  --network mongo \
  --env-file "${ScriptPath}config.env" \
  -v "${ScriptPath}data:/app/data" \
  --name $containerName \
  -d $registryURL:$branch

version=$(docker inspect --format='{{index .Config.Labels "version"}}' $registryURL:$branch)
echo -e "\e[32m ✔  Started version \e[1m$version\e[0m\e[32m from \e[1m$branch\e[0m"

# post to the portalURL and echo only the answer
if [ ! -z "$portalURL" ]; then
  portalResult=$(curl -s -d "" $portalURL)
  if [ "$portalResult" != "true" ]; then
    echo -e "\e[31m ✘  Failed to notify AuthPortal: \e[1m${portalResult}\e[0m"
  else
    echo -e "\e[32m ✔  Notified AuthPortal\e[0m"
  fi
fi
```

## Roadmap
