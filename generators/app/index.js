const Generator = require('yeoman-generator')
const chalk = require('chalk')
const yosay = require('yosay')

module.exports = class extends Generator {
  prompting() {
    this.log(yosay(chalk.red('Server Generator') + '\n Project will be generated in the current folder!'))

    const prompts = [
      {
        type: 'input',
        name: 'name',
        message: 'Package Name (only lower case letters and _)',
        default: this.appname
      },
      {
        type: 'input',
        name: 'title',
        message: 'Project Title',
        default: this.appname
      },
      {
        type: 'input',
        name: 'author',
        message: 'Projects Author'
      },
      {
        type: 'input',
        name: 'envprefix',
        message: 'Prefix for Process Variables',
        default: this.appname
      },
      {
        type: 'input',
        name: 'mongoname',
        message: 'Mongo DB Title',
        default: this.appname
      },
      {
        type: 'input',
        name: 'giturl',
        message: 'Repository URL *',
        default: ''
      },
      {
        type: 'input',
        name: 'dockerregistry',
        message: 'Docker Registry URL *',
        default: ''
      },
      {
        type: 'checkbox',
        name: 'options',
        message: 'zusätzliche Optionen',
        choices: [
          { value: 'webfonts', name: 'Webfonts (Inter, FA)' }
        ]
      }
    ]

    return this.prompt(prompts).then(props => { this.props = props })
  }

  writing () {
    this.props.destinationPath = this.destinationRoot('./')
    const options = {
      webfonts: this.props.options.includes('webfonts')
    }

    this.fs.copy(this.templatePath('./basic/**'), this.destinationRoot('./'), { globOptions: { dot: true } })

    const templates = [
      'package.json',
      '.env.sentry-build-plugin',
      'app/config/index.js',
      'app/config/main.config.json',
      'app/libraries/errorhandling.js',
      'app/frontend/layout/head.pug',
      'app/frontend/layout/layout.pug',
      'app/frontend/layout/loading.html',
      'app/frontend/layout/404.html',
      'tools/publish.sh',
      'README.md'
    ]

    for (let i = 0; i < templates.length; i++) {
      this.fs.copyTpl(
        this.templatePath('./basic/' + templates[i]),
        this.destinationPath(templates[i]),
        { ...this.props, options }
      )
    }

    if (options.webfonts) {
      this.fs.copy(this.templatePath('./webfonts/webfonts/**'), this.destinationPath('./app/static/webfonts/'), { globOptions: { dot: true } })
      this.fs.copy(this.templatePath('./webfonts/*.css'), this.destinationPath('./app/static/styles/'), { globOptions: { dot: true } })
    }
  }

  end () {
    this.spawnCommandSync('git', ['init']);
    this.spawnCommandSync('git', ['remote', 'add', 'origin', this.props.giturl]);
    return
    this.spawnCommandSync('git', ['add', '.']);
    this.spawnCommandSync('git', ['commit', '-m', '"initial commit from generator"']);
    this.spawnCommandSync('git', ['push', '-u', 'origin', 'master']);
  }

  install () {
    this.installDependencies({ bower: false })
  }
}
